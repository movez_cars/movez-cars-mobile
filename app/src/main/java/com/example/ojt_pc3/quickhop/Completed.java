package com.example.ojt_pc3.quickhop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Completed extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed);

    }
    public void ClickUpcoming(View v) {
        Intent myIntent = new Intent(getBaseContext(), Upcoming.class);
        startActivity(myIntent);
        /*going left*/
        overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);
    }

    public void ClickAllRides(View v) {
        Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(myIntent);
        /*going left*/
        overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);
    }
    public void Trip(View v) {
        Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(myIntent);
    }

}
