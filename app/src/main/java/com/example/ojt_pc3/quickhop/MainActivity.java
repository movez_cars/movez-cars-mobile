package com.example.ojt_pc3.quickhop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void ClickCompleted(View v) {

        Intent myIntent = new Intent(getBaseContext(), Completed.class);
        startActivity(myIntent);
        /*going right*/
        overridePendingTransition(R.anim.slide_right, R.anim.slide_out_left);

    }

    public void ClickUpcoming(View v) {

        Intent myIntent = new Intent(getBaseContext(), Upcoming.class);
        startActivity(myIntent);
        /*going right*/
        overridePendingTransition(R.anim.slide_right, R.anim.slide_out_left);

    }
    public void Transaction(View v) {

        Intent myIntent = new Intent(getBaseContext(), allTransactionHisto.class);
        startActivity(myIntent);
    }
}
