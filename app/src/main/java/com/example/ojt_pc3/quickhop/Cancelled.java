package com.example.ojt_pc3.quickhop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Cancelled extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancelled);
    }
    public void ClickAll(View v) {
        Intent myIntent= new Intent (getBaseContext(), allTransactionHisto.class);
        startActivity(myIntent);
        overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);

    }
    public void ClickCredit(View v) {
        Intent myIntent= new Intent (getBaseContext(), Credit.class);
        startActivity(myIntent);
        overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);

    }
    public void ClickDebit(View v) {
        Intent myIntent= new Intent (getBaseContext(), Debit.class);
        startActivity(myIntent);
        overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);

    }
    public void Trip(View v) {
        Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(myIntent);
    }

}
