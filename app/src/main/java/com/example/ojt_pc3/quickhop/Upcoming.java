package com.example.ojt_pc3.quickhop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Upcoming extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming);
  
    }
    public void ClickCompleted(View v) {
        Intent myIntent = new Intent(getBaseContext(), Completed.class);
        startActivity(myIntent);
        /*going right*/
        overridePendingTransition(R.anim.slide_right, R.anim.slide_out_left);

    }

    public void ClickAllRides(View v) {
        Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(myIntent);
        /*going left*/
        overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);
    }
    public void Trip(View v) {
        Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(myIntent);
    }

}
