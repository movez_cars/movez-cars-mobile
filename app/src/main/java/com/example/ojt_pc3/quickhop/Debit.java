package com.example.ojt_pc3.quickhop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Debit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debit);
    }
    public void ClickAll(View v) {
        Intent myIntent= new Intent (getBaseContext(), allTransactionHisto.class);
        startActivity(myIntent);
        overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);

    }
    public void ClickCredit(View v) {
        Intent myIntent= new Intent (getBaseContext(), Credit.class);
        startActivity(myIntent);
        overridePendingTransition(R.anim.slide_left, R.anim.slide_out_right);

    }
    public void ClickCancelled(View v) {
        Intent myIntent= new Intent (getBaseContext(), Cancelled.class);
        startActivity(myIntent);
        overridePendingTransition(R.anim.slide_right, R.anim.slide_out_left);

    }
    public void Trip(View v) {
        Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(myIntent);
    }

}
